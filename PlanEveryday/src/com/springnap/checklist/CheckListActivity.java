package com.springnap.checklist;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.feedback.NotificationType;
import com.feedback.UMFeedbackService;
import com.mobclick.android.MobclickAgent;
import com.mt.airad.AirAD;
import com.springnap.checklist.CheckListCursorAdapter.OnEditModeChangedListener;
import com.springnap.checklist.util.AbstractActivity;

public class CheckListActivity extends AbstractActivity implements
		OnClickListener {
	private static final String LOG_TAG = "CheckListActivity";
	private ChecklistDatabaseHandler mDatabaseHandler;
	private ListView mCheckList;

	private Cursor mCheckListsCursor;
	private CheckListCursorAdapter adapter;
	public static final int NEED_AUTO_FLAOT = 0;
	public static final int WITHOUT_AUTO_FLAOT = 0;
	private ScheduledThreadPoolExecutor mExecutor;
	private boolean mExitable;

	static {
		AirAD.setGlobalParameter("92e881e6-dbea-4a41-88c3-02a3638ab905", false);
	}

	/**
	 * if in copy mode, click on the check list create a new check list as the
	 * copy of clicked one.
	 */
	public static boolean bCopyMode = false;

	@Override
	protected void onResume() {
		// control the tips.
		if (adapter.getCount() == WITHOUT_AUTO_FLAOT) {
			findViewById(R.id.tips).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.tips).setVisibility(View.GONE);
		}
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == NEED_AUTO_FLAOT) {
			refreshDataWithAutoFloat();
		}

		if (requestCode == WITHOUT_AUTO_FLAOT) {
			refreshDataWithoutAutoFloat();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void refreshDataWithAutoFloat() {
		if (adapter != null) {
			mCheckListsCursor = mDatabaseHandler.getTodoListCursor();
			adapter.changeCursor(mCheckListsCursor);

			mCheckList.setSelection(adapter.getCount() - 1);

			if (adapter.getCount() == 1) {
				findViewById(R.id.tips).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.tips).setVisibility(View.GONE);
			}
		} else {
			findViewById(R.id.tips).setVisibility(View.VISIBLE);
		}
	}

	/**
	 * won't auto float to latest item.
	 */
	public void refreshDataWithoutAutoFloat() {
		if (adapter != null) {
			mCheckListsCursor = mDatabaseHandler.getTodoListCursor();
			adapter.changeCursor(mCheckListsCursor);

			if (adapter.getCount() == 0) {
				findViewById(R.id.tips).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.tips).setVisibility(View.GONE);
			}
		} else {
			findViewById(R.id.tips).setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		MobclickAgent.update(this);
		MobclickAgent.onError(this);
		UMFeedbackService.enableNewReplyNotification(this,
				NotificationType.AlertDialog);
		mExecutor = new ScheduledThreadPoolExecutor(1);
		mDatabaseHandler = ChecklistDatabaseHandler.getInstance(this);
		super.onCreate(savedInstanceState);
		mCheckList = (ListView) findViewById(R.id.checkListView);

		mCheckList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				String checklist_id = arg1.getTag().toString();
				if (((CheckListCursorAdapter) arg0.getAdapter()).getEditMode() == true) {
					return;
				} else {
					/**
					 * if not, click on the check list will open the detail
					 * page.
					 */
					Intent openCheckListDetail = new Intent(
							CheckListActivity.this,
							CheckListDetailActivity.class);
					openCheckListDetail.putExtra(CheckListDetailActivity.MODE,
							CheckListDetailActivity.MODE_OPEN_SAVED);
					openCheckListDetail.putExtra(
							CheckListDetailActivity.CHECKLIST_ID, checklist_id);
					CheckListActivity.this.startActivityForResult(
							openCheckListDetail, WITHOUT_AUTO_FLAOT);
				}
			}
		});
		this.registerForContextMenu(mCheckList);

		mCheckListsCursor = mDatabaseHandler.getTodoListCursor();
		adapter = new CheckListCursorAdapter(this, R.layout.checklist_layout,
				mCheckListsCursor, mCheckList);
		mCheckList.setAdapter(adapter);

		/** Listener for edit mode change. */
		OnEditModeChangedListener listener = new OnEditModeChangedListener() {
			@Override
			public void onEditModeDisabled() {
			}

			@Override
			public void onEditModeEnabled() {
			}
		};

		adapter.setOnEditModeEnabledListener(listener);
		this.findViewById(R.id.btn_add_item).setOnClickListener(this);
		this.findViewById(R.id.btn_edit).setOnClickListener(this);
		this.findViewById(R.id.tips).setOnClickListener(this);

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.checklist_context_menu, menu);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		mCheckListsCursor.moveToPosition((int) info.position);
		final String checklistId = mCheckListsCursor
				.getString(mCheckListsCursor.getColumnIndex(DBC.ID));
		final String checklistName = mCheckListsCursor
				.getString(mCheckListsCursor
						.getColumnIndex(DBC.CheckListsTable.TITLE));

		switch (item.getItemId()) {
		case R.id.menu_rename:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(this
					.getString(R.string.CheckListView_CHANGE_ITEM_TEXT));
			LayoutInflater mInflater = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final LinearLayout view = (LinearLayout) mInflater.inflate(
					R.layout.input_box, null);
			builder.setView(view);
			final EditText editbox = (EditText) view.findViewById(R.id.input);
			editbox.setText(checklistName);
			editbox.setSelection(checklistName.length());

			builder.setPositiveButton(this.getString(R.string.Generic_EDIT),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String newTitle = editbox.getEditableText()
									.toString();
							mDatabaseHandler.changeTitle(
									DBC.CheckListsTable.TABLE_NAME,
									DBC.CheckListsTable.TITLE, checklistId,
									newTitle);

							// need refresh the check list when
							// return from the dialog.
							refreshDataWithoutAutoFloat();
						}
					});

			builder.setNegativeButton(this.getString(R.string.Generic_CANCEL),
					null);
			builder.create();
			builder.show();
			openKeyboard();
			return true;
		case R.id.menu_delete:

			mDatabaseHandler.removeChecklist(checklistId);
			refreshDataWithoutAutoFloat();
			return true;
		default:
			return super.onContextItemSelected(item);
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (adapter.getEditMode()) {
				adapter.setEditMode();
				adapter.notifyDataSetChanged();
				return true;
			}

			if (mExitable == false) {
				Toast.makeText(this, this.getString(R.string.exit_warning),
						Toast.LENGTH_SHORT).show();
				mExitable = true;
				Runnable exit_run = new Runnable() {
					@Override
					public void run() {
						mExitable = false;
					}
				};
				mExecutor.schedule(exit_run, 2, TimeUnit.SECONDS);
				return true;
			}

			if (mCheckListsCursor != null) {
				mCheckListsCursor.close();
			}

			adapter.releaseCursor();
			super.onDestroy();
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public int getContentLayout() {
		return R.layout.checklists_layout;
	}

	@Override
	public CharSequence getTitleText() {
		return this.getString(R.string.CheckListView_TITLE);
	}

	@Override
	protected void onDestroy() {
		mDatabaseHandler.releaseDatabase();
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_add_item:
		case R.id.tips:
			// Open the check list detail page.
			Intent openChecklistDetail = new Intent(CheckListActivity.this,
					CheckListDetailActivity.class);
			openChecklistDetail.putExtra(CheckListDetailActivity.MODE,
					CheckListDetailActivity.MODE_CREATE_NEW);
			CheckListActivity.this.startActivityForResult(openChecklistDetail,
					NEED_AUTO_FLAOT);

			break;
		case R.id.btn_edit:
			// enter edit mode.
			adapter.setEditMode();
			adapter.notifyDataSetChanged();
			break;
		}
	}

	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) CheckListActivity.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}

}
