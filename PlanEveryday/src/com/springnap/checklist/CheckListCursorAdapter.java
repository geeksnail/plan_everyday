package com.springnap.checklist;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.springnap.checklist.util.DebugLog;

public class CheckListCursorAdapter extends BaseAdapter {
	public static final String TAG = "CheckListCursorAdaptor";
	private boolean mInEditMode = false;
	private int mLayout = 0;
	private Activity mBoundActivity = null;
	private LayoutInflater mInflater = null;
	private ListView mListView;
	public Cursor cursor;
	private OnEditModeChangedListener mEditModeListener;
	private ChecklistDatabaseHandler mDatabaseHandler = null;

	/** Listener for edit mode enabled. */
	interface OnEditModeChangedListener {
		void onEditModeEnabled();
		void onEditModeDisabled();
	}
	
	public CheckListCursorAdapter(Activity activity, int layout, Cursor c, ListView listview
			) {
		super();
		this.mLayout = layout;
		this.mBoundActivity = activity;
		mListView = listview;
		cursor = c;
		mInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mDatabaseHandler = ChecklistDatabaseHandler.getInstance(activity);
	}
	
	public String getNewChecklistName(String copyedName){
		int tempMax=-1;
		//Reveres all the check list names
		Cursor cur_checklist = mDatabaseHandler.getCursor(DBC.CheckListsTable.TABLE_NAME);
		while(cur_checklist.moveToNext()){
			String titleName = cur_checklist.getString(cur_checklist.getColumnIndex(DBC.CheckListsTable.TITLE));
			if(titleName.contains(copyedName+"-")){
				int sequence = Integer.parseInt(String.valueOf(titleName.charAt((copyedName+"-").length())));
				if(sequence > tempMax){
					tempMax = sequence;
				}
			}
		}
		cur_checklist.close();
		if(tempMax == -1){
			return copyedName+"-1";
		}else{
			return copyedName+"-"+(tempMax+1);
		}
			
	}

	public void duplicateCheckList(String checklistId) {
		/**
		 * only in the edit mode,click
		 * the copy list will create a new check list as a copy.
		 */
		mDatabaseHandler.beginTransaction();
		/** copy the check list. */
		Cursor cur_checklist = mDatabaseHandler.getCheckListByID(checklistId);

		String new_checklist_id = "";
		if (cur_checklist != null && cur_checklist.moveToNext()) {
			Object[] inputObjArray = { getNewChecklistName(cur_checklist.getString(cur_checklist
					.getColumnIndex(DBC.CheckListsTable.TITLE))) };
			new_checklist_id = Long.toString(mDatabaseHandler.addRow(
					DBC.CheckListsTable.CHECKLIST_COLUMNS,
					DBC.CheckListsTable.TABLE_NAME, inputObjArray));
		}
		if (cur_checklist != null) {
			cur_checklist.close();
		}

		//get the group ids related to the old check list.
		/** copy the relationship. */
		Cursor cur_relation = mDatabaseHandler.getRelationshipByID(checklistId);
		while (cur_relation != null && cur_relation.moveToNext()) {

			Object[] inputObjArray = {
					new_checklist_id,
					cur_relation.getString(cur_relation.getColumnIndex(DBC.ItemGroupCheckListTable.ITEM_ID)),
					cur_relation.getInt(cur_relation.getColumnIndex(DBC.ItemGroupCheckListTable.GROUP_ID)),
					cur_relation.getString(cur_relation.getColumnIndex(DBC.ItemGroupCheckListTable.IS_CHECKED))
			};
			mDatabaseHandler.addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,DBC.ItemGroupCheckListTable.TABLE_NAME, inputObjArray);
		}

		/** Update cursor. */
		Cursor checkListsCursor = mDatabaseHandler.getTodoListCursor();
		mDatabaseHandler.endTransaction();
		changeCursor(checkListsCursor);
		/** refresh the list. */
		CheckListCursorAdapter.this.notifyDataSetChanged();

	}

	public void changeCursor(Cursor checkListsCursor) {
		if(cursor != null){
			cursor.close();
		}
		cursor = checkListsCursor;
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (DebugLog.ENABLED) {
			Log.d(TAG, "bindView:" + String.valueOf(cursor.getPosition()));
		}
		
		cursor.moveToPosition(position);
		
		view = mInflater.inflate(mLayout, null);
		final String checklistName = cursor.getString(cursor
				.getColumnIndex(DBC.CheckListsTable.TITLE));
		final String checklistId = cursor.getString(cursor
				.getColumnIndex(DBC.ID));

		if (mInEditMode) {
			// show the minus icon, and add click listener to it.
			view.findViewById(R.id.delete_checklist).setVisibility(View.VISIBLE);

			// add the delete icon to the list.
			view.findViewById(R.id.delete_checklist).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(final View v) {
							
							// prompt the user to delete this
							// checklist.
							AlertDialog.Builder builder = new AlertDialog.Builder(
									mBoundActivity);
							builder.setMessage(v.getContext().getString(
									R.string.CheckListView_DELETE_CONFIRM)+"\n"
									+ checklistName);
							builder.setPositiveButton(v.getContext().getString(
											R.string.Generic_DELETE),
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											//  delete the specific
											// check
											// list.
											ChecklistDatabaseHandler databaseHandler = ChecklistDatabaseHandler
													.getInstance(v
															.getContext());
											databaseHandler
													.removeChecklist(checklistId);
											// need refresh the check list
											// when
											// return from the dialog.
											((CheckListActivity) mBoundActivity).refreshDataWithoutAutoFloat();
										}
									});

							builder.setNegativeButton(
									v.getContext().getString(
											R.string.Generic_CANCEL),
									null);
							builder.create();
							builder.show();
						}

					});


			// Set Listener for text view to change check list title.
			TextView checkList_title = (TextView) view.findViewById(R.id.checklist_title);
			checkList_title.setClickable(true);
			checkList_title.setBackgroundResource(R.drawable.editbox);
			checkList_title.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(mBoundActivity);
					builder.setMessage(mBoundActivity.getString(R.string.CheckListView_CHANGE_ITEM_TEXT));
					final LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.input_box, null);
					builder.setView(view);
					final EditText editbox = (EditText) view.findViewById(R.id.input);
					editbox.setText(checklistName);
					editbox.setSelection(checklistName.length());
					
					builder.setPositiveButton(v.getContext().getString(
							R.string.Generic_EDIT),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int whichButton) {
									
									String newTitle = editbox.getEditableText().toString();
									ChecklistDatabaseHandler databaseHandler = ChecklistDatabaseHandler
											.getInstance(v.getContext());
									databaseHandler.changeTitle(
											DBC.CheckListsTable.TABLE_NAME,
											DBC.CheckListsTable.TITLE,
											checklistId, newTitle);

									// need refresh the check list when
									// return from the dialog.
									((CheckListActivity) mBoundActivity).refreshDataWithoutAutoFloat();
								}
							});

					builder.setNegativeButton(v.getContext().getString(R.string.Generic_CANCEL), null);
					builder.create();
					builder.show();
					openKeyboard();
				}

			});

		} else {
			view.findViewById(R.id.delete_checklist).setVisibility(View.GONE);

			// Delete the listener on check list name.
			TextView checkList_title = (TextView) view.findViewById(R.id.checklist_title);
			checkList_title.setClickable(false);
			checkList_title.setBackgroundDrawable(null);
		}

		// bind the view with cursor value.
		((TextView) view.findViewById(R.id.checklist_title))
				.setText(cursor.getString(cursor
						.getColumnIndex(DBC.CheckListsTable.TITLE)));

		/** Set the checked/total item numbers. */
		((TextView) view.findViewById(R.id.todo_quantity)).setText(String
				.valueOf(mDatabaseHandler.getCheckedItemNum(checklistId)));

		((TextView) view.findViewById(R.id.total_quantity)).setText(String
				.valueOf(mDatabaseHandler.getTotalItemNum(checklistId)));

		/** attach the id with the view for item click event. */
		view.setTag(cursor.getString(cursor.getColumnIndex(DBC.ID)));
		return view;
	}

	@Override
	public int getCount() {
		// The last row is create_new_checklist button.
		return cursor.getCount();
	}

	public void setEditMode() {
		mInEditMode = !mInEditMode;
		if (mInEditMode == true && mEditModeListener != null) {
			mEditModeListener.onEditModeEnabled();
			
		} else if (mInEditMode == false && mEditModeListener != null) {
			mEditModeListener.onEditModeDisabled();
		}
	}

	public boolean getEditMode() {
		return mInEditMode;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	public void setOnEditModeEnabledListener(OnEditModeChangedListener l) {
		mEditModeListener = l;
	}
	
	public void releaseCursor(){
		Log.d(TAG, "releaseCursor()");
		if(cursor != null){
			cursor.close();
		}
	}
	
	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) mBoundActivity
				        .getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}
}
