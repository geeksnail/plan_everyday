package com.springnap.checklist;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.feedback.UMFeedbackService;
import com.mobclick.android.MobclickAgent;
import com.springnap.checklist.CheckListDetailAdapter.OnEditModeChangedListener;
import com.springnap.checklist.util.AbstractActivity;

public class CheckListDetailActivity extends AbstractActivity implements
		OnClickListener {
	public static final String TAG = "CheckListDetailActivity";
	public static final String CHECKLIST_ID = "checklist_id";

	public static final String MODE = "mode";
	public static final String MODE_CREATE_NEW = "create_new";
	public static final String MODE_OPEN_SAVED = "open_saved";

	private ChecklistDatabaseHandler mDatabaseHanlder;
	private ExpandableListView mCheckList_detail;
	private CheckListDetailAdapter adapter;
	private String checklist_name;
	private String mCheckListId;
	private String mode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mode = this.getIntent().getStringExtra(MODE);
		if (mode.equals(MODE_CREATE_NEW)) {
			super.onCreate(savedInstanceState);
			mCheckListId = mDatabaseHanlder
					.createNewCheckList(new Object[] { checklist_name });
		} else if (mode.equals(MODE_OPEN_SAVED)) {
			mCheckListId = this.getIntent().getStringExtra(CHECKLIST_ID);

			/**
			 * This clause must under the above code, for the super's setTitle()
			 * should be invoked under check list id has been retrieved.
			 */
			super.onCreate(savedInstanceState);
		} else {
			return;
		}

		// set the grouped items.
		adapter = new CheckListDetailAdapter(this, mCheckListId);
		mCheckList_detail = ((ExpandableListView) findViewById(R.id.groupsList));
		mCheckList_detail.setAdapter(adapter);
		mCheckList_detail.setGroupIndicator(null);

		mCheckList_detail
				.setOnGroupCollapseListener(new OnGroupCollapseListener() {

					@Override
					public void onGroupCollapse(int groupPosition) {
						mDatabaseHanlder.updateGroupExpandStatus(String
								.valueOf(adapter.getGroupId(groupPosition)),
								false);
					}

				});

		mCheckList_detail.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
				mDatabaseHanlder.updateGroupExpandStatus(
						String.valueOf(adapter.getGroupId(groupPosition)), true);
			}

		});

		// Expand favorite group as default, should be placed after the listener
		// has been setup.
		if (mode.equals(MODE_CREATE_NEW)) {
			mCheckList_detail.expandGroup(0);
		}

		final EditText et_checklist_name = (EditText) findViewById(R.id.checklist_name);
		et_checklist_name.setText(checklist_name);
		et_checklist_name.setSelection(checklist_name.length());

		OnEditModeChangedListener listener = new OnEditModeChangedListener() {

			@Override
			public void onEditModeEnabled() {
				// Show the edit box for change checklist name.
				et_checklist_name.setVisibility(View.VISIBLE);

				// Expand all the groups�� cost performance.
				ArrayList<Group> groups = adapter.getmGroupList();
				// int nonGroupNum = adapter.getmNonGroupItemList().size();
				for (int i = 0; i < groups.size(); i++) {
					mCheckList_detail.expandGroup(i);
				}
			}

			@Override
			public void onEditModeDisabled() {
				// Remove edit box for checklist name.
				et_checklist_name.setVisibility(View.GONE);

				String new_checklist_name = et_checklist_name.getEditableText()
						.toString();
				mDatabaseHanlder.updateChecklistName(mCheckListId,
						new_checklist_name);

				TextView titleText = (TextView) findViewById(R.id.title_text);
				titleText.setText(new_checklist_name);

			}
		};

		// Set the call-back for go into the edit mode, e.g. we need
		// to expand all the groups.
		adapter.setOnEditModeEnabledListener(listener);

		// Get the groups & status of groups ,set the expanded status.
		ArrayList<Group> groups = adapter.getmGroupList();
		// int nonGroupNum = adapter.getmNonGroupItemList().size();
		for (int i = 0; i < groups.size(); i++) {
			boolean isExpanded = mDatabaseHanlder.isExpanded(String
					.valueOf(groups.get(i).getmGroupId()));
			Log.d(TAG,
					"group id:"
							+ String.valueOf(groups.get(i).getmGroupId()
									+ "isExpaned:" + isExpanded));
			if (isExpanded) {
				mCheckList_detail.expandGroup(i);
			}
		}

		this.findViewById(R.id.btn_add_group).setVisibility(View.VISIBLE);
		((ImageView) this.findViewById(R.id.btn_add_group))
				.setImageResource(R.drawable.checklist_icon_add_group);
//		this.findViewById(R.id.btn_add_item_to_group).setVisibility(
//				View.VISIBLE);
//		((ImageView) this.findViewById(R.id.btn_add_item_to_group))
//				.setImageResource(R.drawable.add_item);
		this.findViewById(R.id.btn_add_item).setVisibility(View.INVISIBLE);

		this.findViewById(R.id.btn_add_group).setOnClickListener(this);
//		this.findViewById(R.id.btn_add_item_to_group).setOnClickListener(this);
		this.findViewById(R.id.btn_edit).setOnClickListener(this);

		// registerForContextMenu(mCheckList_detail);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			adapter.reloadData();
			adapter.notifyDataSetInvalidated();
		} else if (resultCode == Activity.RESULT_CANCELED) {
			adapter.reloadData();
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public int getContentLayout() {
		return R.layout.check_list_detail;
	}

	@Override
	public CharSequence getTitleText() {
		mDatabaseHanlder = ChecklistDatabaseHandler.getInstance(this);
		if (mode.equals(MODE_CREATE_NEW)) {
			Cursor cur_checklist = mDatabaseHanlder
					.getCursor(DBC.CheckListsTable.TABLE_NAME);
			checklist_name = this
					.getString(R.string.CheckListView_CREATE_LIST_DEFAULT_NAME)
					+ "\u0020" + (cur_checklist.getCount() + 1);
			cur_checklist.close();
		} else {
			Cursor cur_checklist = mDatabaseHanlder
					.getCheckListById(mCheckListId);
			if (cur_checklist.moveToNext()) {
				checklist_name = cur_checklist.getString(cur_checklist
						.getColumnIndex(DBC.CheckListsTable.TITLE));
			}
			cur_checklist.close();
		}

		return checklist_name;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.checklists_detail_menu, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.uncheckall:
			mDatabaseHanlder.updateCheckStatusByChecklist(mCheckListId, false);
			adapter.reloadData();
			adapter.notifyDataSetChanged();
			break;

		case R.id.checklistdetail_checkall:
			mDatabaseHanlder.updateCheckStatusByChecklist(mCheckListId, true);
			adapter.reloadData();
			adapter.notifyDataSetChanged();
			break;
		case R.id.checklistdetail_view_checked_items:
			adapter.mHideChecked = !adapter.mHideChecked;
			adapter.notifyDataSetChanged();
			break;
		case R.id.feedback:
			UMFeedbackService.openUmengFeedbackSDK(this);
			break;
		}
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (adapter.getEditMode()) {
				adapter.setEditMode();
				adapter.notifyDataSetChanged();
				return true;
			}
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		/*case R.id.btn_add_item_to_group:
			Intent openItem = new Intent(CheckListDetailActivity.this,
					ItemListActivity.class);
			openItem.putExtra(ItemListActivity.REQUEST_MODE,
					ItemListActivity.MODE_ADD_TO_CHECKLIST);
			openItem.putExtra(ItemListActivity.CHECKLIST_ID, mCheckListId);
			openItem.putIntegerArrayListExtra(ItemListActivity.EXISTING_ITEM,
					adapter.getAllItemsForCurrentCheckList());
			CheckListDetailActivity.this.startActivityForResult(openItem, 0);
			break;*/
		case R.id.btn_edit:
			// enter edit mode.
			adapter.setEditMode();
			// refresh the list.
			adapter.notifyDataSetChanged();
			break;
		case R.id.btn_add_group:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setMessage(this.getString(R.string.menu_add_group));
			LayoutInflater mInflater = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final LinearLayout view = (LinearLayout) mInflater.inflate(
					R.layout.input_box, null);
			final EditText editbox = (EditText) view.findViewById(R.id.input);
			editbox.setHint(this.getString(R.string.new_group_hint));

			builder.setView(view);
			builder.setPositiveButton(this.getString(R.string.Generic_EDIT),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							String newTitle = editbox.getEditableText()
									.toString();
							adapter.addGroup(newTitle);
							mCheckList_detail.setSelectedGroup(adapter
									.getGroupCount());
						}
					});

			builder.setNegativeButton(this.getString(R.string.Generic_CANCEL),
					null);
			builder.create();
			builder.show();
			openKeyboard();
			break;
		}
	}

	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) CheckListDetailActivity.this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (adapter.mHideChecked) {
			menu.getItem(2).setIcon(R.drawable.view_checked_items);
			menu.getItem(2).setTitle(R.string.view_checked);
		} else {
			menu.getItem(2).setIcon(R.drawable.hide_checked_items);
			menu.getItem(2).setTitle(R.string.hide_checked);
		}
		return super.onPrepareOptionsMenu(menu);
	}

}
