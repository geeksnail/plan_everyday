package com.springnap.checklist;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.springnap.checklist.util.DebugLog;

public class CheckListDetailAdapter extends BaseExpandableListAdapter {

	public static final String TAG = "CheckListDetailAdapter";
	/** buffer for groups & non-grouped items. */
	public ArrayList<Group> mGroupList = new ArrayList<Group>();
	/**
	 * All items in this check list, used for itemlistActivity to different
	 * already existing items.
	 */
	private ArrayList<Integer> mExistingItems = new ArrayList<Integer>();
	private boolean mInEditMode = false;
	private LayoutInflater mInflater = null;
	private Activity mBoundActivity = null;
	private ChecklistDatabaseHandler mDatabaseHanlder;
	private String mCheckListId = null;
	private OnEditModeChangedListener mEditModeListener;
	public boolean mHideChecked = true;// hide checked items or not.

	/** Listener for edit mode enabled. */
	interface OnEditModeChangedListener {
		void onEditModeEnabled();
		void onEditModeDisabled();
	}

	/** For position changed for group & item. */
	interface OnPositionChangedListener {
		void onGroupPositionChanged(int newGroupPosition);

		void onItemPositionChanged(int x, int y);
	}

	public void setOnEditModeEnabledListener(OnEditModeChangedListener l) {
		mEditModeListener = l;
	}

	/** expose the data buffers. */
	public ArrayList<Group> getmGroupList() {
		return mGroupList;
	}

	public CheckListDetailAdapter(Activity activty, String checkListId) {
		mCheckListId = checkListId;
		mBoundActivity = activty;
		mInflater = (LayoutInflater) activty
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDatabaseHanlder = ChecklistDatabaseHandler.getInstance(activty);
		reloadData();
	}

	public void reloadData() {
		mGroupList.clear();
		mExistingItems.clear();

		/**
		 * load the default groups & items, the default group is the favorite
		 * group.
		 */
		Cursor cur_groups = mDatabaseHanlder
				.getGroupsByCheckListId(mCheckListId);
		Cursor cur_items = null;
		while (cur_groups.moveToNext()) {
			String groupName = cur_groups.getString(cur_groups
					.getColumnIndex(DBC.GroupTable.NAME));
			long groupId = Long.valueOf(cur_groups.getString(cur_groups
					.getColumnIndex(DBC.ID)));
			/** Create a new group. */
			Group group = new Group();
			group.setmTitle(groupName);
			group.setmGroupId(groupId);
			/** Create a new item list for the specific group. */
			ArrayList<Item> itemList = new ArrayList<Item>();
			group.setmItems(itemList);
			mGroupList.add(group);
		}

		cur_items = mDatabaseHanlder.getItemsFromCurrentCheckList(Integer
				.parseInt(mCheckListId));
		int groupId = -2;
		ArrayList<Item> items = null;
		while (cur_items.moveToNext()) {
			Item item = new Item();
			item.setmItemId(Long.valueOf(cur_items.getString(cur_items
					.getColumnIndex(DBC.ItemGroupCheckListTable.ITEM_ID))));
			item.setmTitle(cur_items.getString(cur_items
					.getColumnIndex(DBC.ItemTable.NAME)));
			item.setmIsFovrite(cur_items.getInt(cur_items
					.getColumnIndex(DBC.ItemTable.IS_FAVORITE)) == 1);
			item.setmChecked(cur_items.getInt(cur_items
					.getColumnIndex(DBC.ItemGroupCheckListTable.IS_CHECKED)) == 1);
			int tempGroupId = cur_items.getInt(cur_items
					.getColumnIndex(DBC.ItemGroupCheckListTable.GROUP_ID));

			// if the group id changed, get the item array again, otherwise
			// reuse the old one.
			if (groupId != tempGroupId) {
				items = getItemsByGroupId(tempGroupId);
				groupId = tempGroupId;
			}

			if (items != null) {
				items.add(item);
			}

			// Store the existing item id.
			mExistingItems.add(cur_items.getInt(cur_items
					.getColumnIndex(DBC.ItemGroupCheckListTable.ITEM_ID)));

		}

		if (cur_groups != null) {
			cur_groups.close();
		}
		if (cur_items != null) {
			cur_items.close();
		}

	}

	public ArrayList<Item> getItemsByGroupId(int groupId) {
		for (Group group : mGroupList) {
			if (group.getmGroupId() == groupId) {
				return group.getmItems();
			}
		}
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mGroupList.get(groupPosition).getmItems().get(childPosition)
				.getmTitle();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, final ViewGroup parent) {

		if (DebugLog.ENABLED) {
			Log.d(TAG, "getChildView");
		}

		ViewHolder_child viewHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.child, null);
			viewHolder = new ViewHolder_child();
			viewHolder.checkbox = (CheckBox) convertView
					.findViewById(R.id.checkbox);
			viewHolder.itemName = (TextView) convertView
					.findViewById(R.id.item_name);
			viewHolder.fovrite = (CheckBox) convertView
					.findViewById(R.id.fovrite);
			viewHolder.changeGroup = (ImageView) convertView
					.findViewById(R.id.add_to_group);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder_child) convertView.getTag();
		}

		/** switch of the delete icon. */
		ImageView im_delete_checklist = (ImageView) convertView.findViewById(R.id.delete_checklist);
		if (mInEditMode) {
			// Hide the checkbox and favorite.
			viewHolder.checkbox.setVisibility(View.INVISIBLE);
			viewHolder.fovrite.setVisibility(View.INVISIBLE);

			// show the minus icon, and add click listener to it.
			im_delete_checklist.setVisibility(View.VISIBLE);
			viewHolder.changeGroup.setVisibility(View.VISIBLE);
			im_delete_checklist.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					AlertDialog.Builder builder = new AlertDialog.Builder(
							mBoundActivity);

					builder.setMessage(mBoundActivity
							.getString(R.string.CheckListView_DELETE_CONFIRM)
							+ "\n"
							+ mGroupList.get(groupPosition).getmItems()
									.get(childPosition).getmTitle());

					builder.setPositiveButton(
							v.getContext().getString(R.string.Generic_DELETE),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// Update database
									mDatabaseHanlder.removeItemFromChecklist(
											mCheckListId,
											String.valueOf(mGroupList
													.get(groupPosition)
													.getmItems()
													.get(childPosition)
													.getmItemId()));
									// Update buffer.
									reloadData();

									// Refresh the list.
									CheckListDetailAdapter.this
											.notifyDataSetChanged();

								}
							});

					builder.setNegativeButton(
							v.getContext().getString(R.string.Generic_CANCEL),
							null);
					builder.create();
					builder.show();
				}

			});

			viewHolder.itemName.setBackgroundResource(R.drawable.editbox);
			viewHolder.itemName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					final Item cur_item = mGroupList.get(groupPosition)
							.getmItems().get(childPosition);
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mBoundActivity);

					builder.setMessage(mBoundActivity
							.getString(R.string.CheckListView_CHANGE_ITEM_TEXT));
					final LinearLayout view = (LinearLayout) mInflater.inflate(
							R.layout.input_box, null);
					final EditText editbox = (EditText) view
							.findViewById(R.id.input);
					editbox.setText(cur_item.getmTitle());
					editbox.setSelection(cur_item.getmTitle().length());

					builder.setView(view);
					builder.setPositiveButton(
							v.getContext().getString(R.string.Generic_EDIT),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									String newTitle = editbox.getEditableText()
											.toString();
									ChecklistDatabaseHandler databaseHandler = ChecklistDatabaseHandler
											.getInstance(v.getContext());
									databaseHandler.changeTitle(
											DBC.ItemTable.TABLE_NAME,
											DBC.ItemTable.NAME, String
													.valueOf(cur_item
															.getmItemId()),
											newTitle);

									// need refresh the check list when
									// return from the dialog.
									cur_item.setmTitle(newTitle);
									CheckListDetailAdapter.this
											.notifyDataSetChanged();
								}
							});

					builder.setNegativeButton(
							v.getContext().getString(R.string.Generic_CANCEL),
							null);
					builder.create();
					builder.show();
					openKeyboard();
				}
			});

		} else {
			im_delete_checklist.setVisibility(View.GONE);
			viewHolder.changeGroup.setVisibility(View.GONE);
			// Show the checkbox and favorite.
			viewHolder.checkbox.setVisibility(View.VISIBLE);
			viewHolder.fovrite.setVisibility(View.VISIBLE);

			// remove the click listener.
			viewHolder.itemName.setOnClickListener(null);
			viewHolder.itemName.setBackgroundDrawable(null);
		}

		// get the status of the selector button & favorite button.
		boolean isChecked = mGroupList.get(groupPosition).getmItems()
				.get(childPosition).ismChecked();
		viewHolder.checkbox.setChecked(isChecked);
		if (isChecked && mHideChecked) {
			convertView.setVisibility(View.INVISIBLE);
		} else {
			convertView.setVisibility(View.VISIBLE);
		}
		viewHolder.fovrite.setChecked(mGroupList.get(groupPosition).getmItems()
				.get(childPosition).ismIsFovrite());

		viewHolder.checkbox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				boolean ischecked = ((CheckBox) v).isChecked();
				if (ischecked) {
					((ViewHolder_child)(((View)v.getParent()).getTag())).itemName.setTextColor(Color.rgb(178, 178, 178));
				} else {
					((ViewHolder_child)(((View)v.getParent()).getTag())).itemName.setTextColor(Color.BLACK);
				}
				mGroupList.get(groupPosition).getmItems().get(childPosition)
						.setmChecked(ischecked);

				// update check status for item.
				String itemId = String.valueOf(mGroupList.get(groupPosition)
						.getmItems().get(childPosition).getmItemId());
				mDatabaseHanlder.updateCheckStatus(mCheckListId, itemId,
						ischecked);

				// play the animation that item fly out the screen.
				Animation flyAnimation = AnimationUtils.loadAnimation(
						mBoundActivity, R.anim.item_fly_out);

				flyAnimation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						((View)v.getParent()).setVisibility(View.INVISIBLE);
					}

					@Override
					public void onAnimationRepeat(Animation animation) {

					}

				});

				if (ischecked && mHideChecked) {
					((View)v.getParent()).startAnimation(
							flyAnimation);
				}
			}
		});

		viewHolder.fovrite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mGroupList.get(groupPosition).getmItems().get(childPosition)
						.setmIsFovrite(((CheckBox) v).isChecked());

				// update database.
				String item_id = String.valueOf(mGroupList.get(groupPosition)
						.getmItems().get(childPosition).getmItemId());
				mDatabaseHanlder.updateFavoriteStatus(item_id,
						((CheckBox) v).isChecked());
			}

		});

		viewHolder.changeGroup.setOnClickListener(new OnClickListener() {
			int groupId = -1;
			private int groupIndex;

			@Override
			public void onClick(View v) {

				final Cursor cur_group = mDatabaseHanlder
						.getGroupsByCheckListId(mCheckListId);
				android.content.DialogInterface.OnClickListener ItemClickListener = new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						cur_group.moveToPosition(which);
						groupId = cur_group.getInt(cur_group
								.getColumnIndex(DBC.ID));
						groupIndex = which;
					}

				};
				AlertDialog.Builder builder = new AlertDialog.Builder(
						mBoundActivity);
				builder = builder.setSingleChoiceItems(cur_group, -1,
						DBC.GroupTable.NAME, ItemClickListener);

				android.content.DialogInterface.OnClickListener ButtonClickListener = new android.content.DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							// update database.
							String item_id = String.valueOf(mGroupList
									.get(groupPosition).getmItems()
									.get(childPosition).getmItemId());
							mDatabaseHanlder.addItemToGroup(
									Integer.parseInt(item_id), groupId,
									Integer.parseInt(mCheckListId));

							// 鏇存柊鐣岄潰銆�
							mGroupList
									.get(groupIndex)
									.getmItems()
									.add(mGroupList.get(groupPosition)
											.getmItems().get(childPosition));
							mGroupList.get(groupPosition).getmItems()
									.remove(childPosition);
							notifyDataSetChanged();
						}
					}

				};
				builder = builder.setPositiveButton(
						R.string.Generic_GENERIC_YES, ButtonClickListener);
				builder.setNegativeButton(R.string.Generic_GENERIC_NO, null);
				builder.setTitle(R.string.changegroup_dialog_title).show();
			}

		});

		// If the check box is checked, change the text in grey.
		boolean checked = mGroupList.get(groupPosition).getmItems()
				.get(childPosition).ismChecked();

		if (checked) {
			viewHolder.itemName.setTextColor(Color.rgb(178, 178, 178));
		} else {
			viewHolder.itemName.setTextColor(Color.BLACK);
		}

		viewHolder.itemName.setText((CharSequence) getChild(groupPosition,
				childPosition));

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (DebugLog.ENABLED) {
			Log.d(TAG,
					"getChildrenCount:"
							+ String.valueOf(mGroupList.get(groupPosition)
									.getmItems().size()));
		}
		return mGroupList.get(groupPosition).getmItems().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroupList.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mGroupList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return mGroupList.get(groupPosition).getmGroupId();
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, final ViewGroup parent) {

		if (DebugLog.ENABLED) {
			Log.d(TAG, "getGroupView");
		}
		
		ViewHolder groupHolder;
		if(convertView == null){
			groupHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.group, null);
			groupHolder.addToGroup = (ImageView) convertView.findViewById(R.id.add_to_group);
			groupHolder.arrow = (ImageView) convertView
					.findViewById(R.id.group_arrow);
			groupHolder.titleName = (TextView) convertView
					.findViewById(R.id.group_name);
			groupHolder.deleteIcon = (ImageView) convertView
					.findViewById(R.id.delete_checklist);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (ViewHolder) convertView.getTag();
		}

		// set the arrow picture.
		if (isExpanded) {
			groupHolder.arrow.setImageResource(R.drawable.checklist_icon_collapse);
		} else {
			groupHolder.arrow.setImageResource(R.drawable.checklist_icon_expand);
		}

		/** switch of the delete icon. */
		if (mInEditMode) {
			// show the minus icon, and add click listener to it.
			groupHolder.deleteIcon.setVisibility(View.VISIBLE);
			groupHolder.deleteIcon.setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							AlertDialog.Builder builder;
							builder = new AlertDialog.Builder(mBoundActivity);

							builder.setTitle(mBoundActivity
									.getString(R.string.CheckListView_DELETE_GROUP_QUESTION));

							builder.setPositiveButton(
									v.getContext().getString(
											R.string.Generic_DELETE),
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
											// Update
											// database.
											mDatabaseHanlder.deleteGroupAndItems(String
													.valueOf(mGroupList.get(
															groupPosition)
															.getmGroupId()));

											// Update
											// buffer.
											mGroupList.remove(groupPosition);

											// Refresh
											// the list.
											CheckListDetailAdapter.this
													.notifyDataSetChanged();

										}
									});

							builder.setNegativeButton(
									v.getContext().getString(
											R.string.Generic_CANCEL), null);
							builder.create();
							builder.show();
						}

					});

			groupHolder.titleName.setBackgroundResource(R.drawable.editbox);
			groupHolder.titleName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mBoundActivity);
					builder.setMessage(mBoundActivity
							.getString(R.string.CheckListView_CHANGE_ITEM_TEXT));
					final LinearLayout view = (LinearLayout) mInflater.inflate(
							R.layout.input_box, null);
					builder.setView(view);
					final EditText editbox = (EditText) view
							.findViewById(R.id.input);
					String groupTitle = mGroupList.get(groupPosition)
							.getmTitle();
					editbox.setText(groupTitle);
					editbox.setSelection(groupTitle.length());

					builder.setPositiveButton(
							v.getContext().getString(R.string.Generic_EDIT),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int whichButton) {
									String newTitle = editbox.getEditableText().toString();
									ChecklistDatabaseHandler databaseHandler = ChecklistDatabaseHandler.getInstance(v.getContext());
									databaseHandler.changeTitle(
											DBC.GroupTable.TABLE_NAME,
											DBC.GroupTable.NAME, String
													.valueOf((mGroupList
															.get(groupPosition)
															.getmGroupId())),
											newTitle);

									// need refresh the check list when
									// return from the dialog.
									mGroupList.get(groupPosition).setmTitle(
											newTitle);
									CheckListDetailAdapter.this
											.notifyDataSetChanged();
								}
							});

					builder.setNegativeButton(
							v.getContext().getString(R.string.Generic_CANCEL),
							null);
					builder.create();
					builder.show();

					// pop up soft input borad.
					openKeyboard();
				}

			});

		} else {
			convertView.findViewById(R.id.delete_checklist).setVisibility(
					View.GONE);
			groupHolder.titleName.setBackgroundDrawable(null);
			groupHolder.titleName.setClickable(false);
		}
		
		groupHolder.addToGroup.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent openItem = new Intent(mBoundActivity,ItemListActivity.class);
				openItem.putExtra(ItemListActivity.REQUEST_MODE,ItemListActivity.MODE_ADD_TO_CHECKLIST);
				openItem.putExtra(ItemListActivity.CHECKLIST_ID, mCheckListId);
				openItem.putExtra(ItemListActivity.GROUP_ID, ""+mGroupList.get(groupPosition).getmGroupId());
				openItem.putIntegerArrayListExtra(ItemListActivity.EXISTING_ITEM,getAllItemsForCurrentCheckList());
				mBoundActivity.startActivityForResult(openItem, 0);
			}
		});

		groupHolder.titleName.setText(mGroupList.get(groupPosition).getmTitle());
		return convertView;
	}

	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) mBoundActivity
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public boolean addGroup(String groupName) {

		// Add a new group under this check list.
		long group_id = mDatabaseHanlder
				.createNewGroup(mCheckListId, groupName);

		/** Update buffer. */
		Group group = new Group();
		group.setmGroupId(group_id);
		group.setmTitle(groupName);
		ArrayList<Item> itemList = new ArrayList<Item>();
		group.setmItems(itemList);
		mGroupList.add(group);
		return true;
	}

	public void setEditMode() {
		mInEditMode = !mInEditMode;
		if (mInEditMode == true && mEditModeListener != null) {
			mEditModeListener.onEditModeEnabled();
		} else if (mInEditMode == false && mEditModeListener != null) {
			mEditModeListener.onEditModeDisabled();
		}
	}

	public boolean getEditMode() {
		return mInEditMode;
	}

	/**
	 * Get the items id belong to current check list.
	 * 
	 * @return
	 */
	public ArrayList<Integer> getAllItemsForCurrentCheckList() {
		return mExistingItems;
	}

	class ViewHolder {
		public ImageView arrow;
		public ImageView deleteIcon;
		public TextView titleName;
		public ImageView addToGroup;
	}

	class ViewHolder_child {
		public CheckBox checkbox;
		public TextView itemName;
		public CheckBox fovrite;
		public ImageView changeGroup;
		public ImageView addToGroup;
	}

	public void setmGroupList(ArrayList<Group> mGroupList) {
		this.mGroupList = mGroupList;
	}

}
