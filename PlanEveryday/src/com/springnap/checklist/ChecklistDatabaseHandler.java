package com.springnap.checklist;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.springnap.checklist.util.DebugLog;

public class ChecklistDatabaseHandler {

	private static final String LOG_TAG = "ChecklistDatabaseHandler";
	private static final int DATABASE_VERSION = 1;
	private static ChecklistDatabaseHandler sDataBaseHandler = null;
	private static SQLHelper sDBHelper = null;
	private static SQLiteDatabase sDataBase = null;
	private Context mContext;

	private ChecklistDatabaseHandler(Context context) {
		sDBHelper = new SQLHelper(context);
		mContext = context;
	}

	public static ChecklistDatabaseHandler getInstance(Context context) {
		if (sDataBaseHandler == null) {
			sDataBaseHandler = new ChecklistDatabaseHandler(context);
			sDataBase = sDBHelper.getWritableDatabase();
		}
		return sDataBaseHandler;
	}

	static class SQLHelper extends SQLiteOpenHelper {

		public SQLHelper(Context context) {
			super(context, DBC.DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}

	// -----------------------------------------------------------------------------------
	// -----------------------------------------------------------------------------------

	public boolean checkIfPopulated(String dBName) {
		Cursor c = null;
		try {
			c = sDataBase.rawQuery(DBC.SELECT + DBC.FROM + dBName, null);
			if (c.getCount() > 0) {
				return true;
			}
		} catch (SQLiteException e) {
			return false;
		} finally {
			c.close();
		}
		return false;
	}

	public void createTable(DBColumn[] dBTable, String dBName) {
		// Create the database_table if it does not exist
		String createTableString = DBC.getCreateTableString(dBName, dBTable);
		sDataBase.execSQL(createTableString);
	}

	// Set values to the Object array representing the row to insert
	public long addRow(DBColumn[] dBColumns, String table, Object[] inputObjArray) {

		// Get a ContentValues map with key/value pairs of a
		// table_column_names/values, where values is the supplied Object array.
		ContentValues cv = DBC.getContentValues(dBColumns, inputObjArray);

		// Insert a row of values into database using the contentvalues
		return sDataBase.insert(table, dBColumns[1].getColumn(), cv);
	}

	public Cursor getCursor(String tableName) {

		Cursor c = null;
		c = sDataBase.rawQuery(DBC.SELECT + DBC.STAR + DBC.FROM + tableName, null);
		return c;

	}
	
	public Cursor getCursor(String tableName,String orderField) {

		Cursor c = null;
		c = sDataBase.rawQuery(DBC.SELECT + DBC.STAR + DBC.FROM + tableName + " order by upper(" + orderField + ")", null);
		return c;

	}


	public String[] getColumnNames(DBColumn[] columns) {
		String[] s = new String[columns.length];

		for (int i = 0; i < columns.length; i++) {
			s[i] = columns[i].getColumn();
		}
		return s;
	}

	// gets the cursor containing the data for checklists view
	public Cursor getTodoListCursor() {
		String query_str = "select * from checklists";

		if (DebugLog.ENABLED) {
			Log.d(LOG_TAG, query_str);
		}
		Cursor c = null;
		c = sDataBase.rawQuery(query_str, null);
		return c;
	}

	public int getTotalItemNum(String checklistid) {
		String query_str = "select distinct item_id from itemGroupCheckList where item_id != '-1' and checklists_id ="
				+ checklistid;

		if (DebugLog.ENABLED) {
			Log.d(LOG_TAG, query_str);
		}
		Cursor c = null;
		c = sDataBase.rawQuery(query_str, null);
		int totalNum = c.getCount();
		c.close();
		return totalNum;
	}

	public int getCheckedItemNum(String checklistid) {
		String query_str = "select distinct item_id from itemGroupCheckList where is_checked=1 and item_id > -1 and checklists_id ="
				+ checklistid;

		if (DebugLog.ENABLED) {
			Log.d(LOG_TAG, query_str);
		}
		Cursor c = null;
		c = sDataBase.rawQuery(query_str, null);
		int checkedNum = c.getCount();
		c.close();
		return checkedNum;
	}

	/**
	 * Get groups by linked by check list.
	 * 
	 * @param checkListId
	 * @return
	 */
	public Cursor getGroupsByCheckListId(String checkListId) {
		String query_str = "SELECT distinct groups.*  FROM itemGroupCheckList join groups on groups._id = itemGroupCheckList.group_id and itemGroupCheckList.checklists_id="
				+ checkListId;

		return sDataBase.rawQuery(query_str, null);
	}

	/**
	 * Get items linked by group.
	 * 
	 * @param checkListId
	 * @return
	 */
	public Cursor getItemsByGroupId(String groupId) {
		String query_str = "SELECT distinct * FROM itemGroupCheckList join item on item._id = itemGroupCheckList.item_id and itemGroupCheckList.group_id="
				+ groupId;

		return sDataBase.rawQuery(query_str, null);
	}
	
	public Cursor getItemsFromCurrentCheckList(int checklistId) {
		String query_str = "SELECT distinct * FROM itemGroupCheckList join item on itemGroupCheckList.item_id = item._id where checklists_id="+checklistId;
		return sDataBase.rawQuery(query_str, null);
	}
	
	/**
	 * remove a row in checklist.
	 * 
	 * @param checkListID
	 * @return
	 */
	public boolean removeChecklist(String checkListID) {
		String deleteStr = "delete from checkLists where _id =" + "'" + checkListID + "'";
		String deleteRelation = "delete from " + DBC.ItemGroupCheckListTable.TABLE_NAME
				+ " where checklists_id =" + "'" + checkListID + "'";
		sDataBase.execSQL(deleteStr);
		sDataBase.execSQL(deleteRelation);
		return true;
	}

	/**
	 * get cursor through id
	 * 
	 * @param id
	 *            check list id
	 * @return
	 */
	public Cursor getCheckListByID(String checkListID) {
		String str = "select * from " + DBC.CheckListsTable.TABLE_NAME + " where\u0020" + DBC.ID
				+ "=" + "'" + checkListID + "'";
		return sDataBase.rawQuery(str, null);
	}

	/**
	 * get relation ship by check list id.
	 * 
	 * @param checkListID
	 * @return
	 */
	public Cursor getRelationshipByID(String checkListID) {
		String str = "select * from " + DBC.ItemGroupCheckListTable.TABLE_NAME + " where\u0020"
				+ DBC.ItemGroupCheckListTable.CHECKLISTS_ID + "=" + "'" + checkListID + "'";
		return sDataBase.rawQuery(str, null);
	}

	/**
	 * trigger for delete checklist.
	 * 
	 * @return
	 */
	public boolean createTriggerForCheckListRemove() {
		String triggerForCheckListRemove = "create trigger if not exists chelistRemoveTrigger DELETE on checkLists"
				+ "\u0020BEGIN\u0020"
				+ "\u0020DELETE FROM itemGroupCheckList where checklists_id = old._id;\u0020"
				+ "\u0020END\u0020";
		sDataBase.execSQL(triggerForCheckListRemove);
		return false;
	}

	/**
	 * trigger for delete ItemGroupCheckList.
	 * 
	 * @return
	 */
	public boolean createTriggerForItemGroupCheckListRemove() {
		String triggerForCheckListRemove = "create trigger if not exists itemGroupCheckListRemoveTrigger DELETE on itemGroupCheckList"
				+ "\u0020BEGIN\u0020"
				+ "\u0020DELETE FROM groups where _id = old.group_id and old.group_id != 1;\u0020"
				/*
				 * +
				 * "\u0020DELETE FROM item where _id = old.item_id and item.is_favorite = 0;\u0020"
				 */
				+ "\u0020END\u0020";
		sDataBase.execSQL(triggerForCheckListRemove);
		return false;
	}

	/**
	 * populate the predefined data to the table.
	 */
	public void populatePredefinedItems() {

		// favorite item number, for the performance issue.
		// int[] favorite_sequence_num =
		// {26,43,49,54,58,59,60,73,74,81,92,93,99,102,103,116};

		String key = null;
		Integer resID = -1;
		boolean isFovrite = false;

		sDataBase.beginTransaction();
		// load the predefined items.
		for (int i = 0; i < 10; i++) {
			key = "CheckListView_PREDEFINED_ITEM_" + (i + 1);
			/*
			 * for(int j=0;j<favorite_sequence_num.length;j++){ if(i+1 ==
			 * favorite_sequence_num[j]){ key = "CheckListView_PREDEFINED_ITEM_"
			 * + (i + 1) + "_Favourite"; break; } }
			 */
			
			try {
				resID = (Integer) R.string.class.getField(key).get(null);
				String title = null;
				if (resID == -1) {
					title = "";
				} else {
					title = mContext.getString(resID);
					resID = -1;
				}

				isFovrite = false;
				int fovarite = (isFovrite == true ? 1 : 0);
				addRow(DBC.ItemTable.ITEMSLIST_COLUMNS, DBC.ItemTable.TABLE_NAME, new Object[]{
						title, fovarite});

			} catch (IllegalArgumentException e) {
				
			} catch (SecurityException e) {
				
			} catch (IllegalAccessException e) {
				
			} catch (NoSuchFieldException e) {
				
				key = "CheckListView_PREDEFINED_ITEM_" + (i + 1) + "_Favourite";
				try {
					resID = (Integer) R.string.class.getField(key).get(null);
				} catch (IllegalArgumentException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				} catch (SecurityException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				} catch (IllegalAccessException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				} catch (NoSuchFieldException ex) {
					ex.printStackTrace();
				}

				String title = null;
				if (resID == -1) {
					title = "";
				} else {
					title = mContext.getString(resID);
					resID = -1;
				}

				isFovrite = true;
				int fovarite = (isFovrite == true ? 1 : 0);
				addRow(DBC.ItemTable.ITEMSLIST_COLUMNS, DBC.ItemTable.TABLE_NAME, new Object[]{
						title, fovarite});
			}
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
	}

	public String createNewCheckList(Object[] checklistValue) {
		/** open a transaction for performance. */
		sDataBase.beginTransaction();
		long newChecklistID = addRow(DBC.CheckListsTable.CHECKLIST_COLUMNS,
				DBC.CheckListsTable.TABLE_NAME, checklistValue);

		// Create a favorite group for the current checklist.
		int resID = -1;
		try {
			resID = (Integer) R.string.class.getField(
					"CheckListView_CREATE_LIST_DEFAULT_GROUP_NAME").get(null);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long group_id = addRow(DBC.GroupTable.GROUP_COLUMNS, DBC.GroupTable.TABLE_NAME,
				new Object[]{mContext.getString(resID)});

		// save the grouped items & groups.
		Cursor mCurItem = getFavoriteItems();
		while (mCurItem.moveToNext()) {
				addRow(
						DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,
						DBC.ItemGroupCheckListTable.TABLE_NAME,
						new Object[]{
								String.valueOf(newChecklistID),
								String.valueOf(mCurItem.getString(mCurItem.getColumnIndex(DBC.ID))),
								(int) group_id, 0});
			
		}

		mCurItem.close();
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();

		return String.valueOf(newChecklistID);
	}

	public Cursor getFavoriteItems(){
		Cursor c = null;
		c = sDataBase.rawQuery(DBC.SELECT + DBC.STAR + DBC.FROM + DBC.ItemTable.TABLE_NAME+" WHERE "+DBC.ItemTable.IS_FAVORITE+"=1", null);
		return c;
	}
	/**
	 * Create a new group under specific check list.
	 * 
	 * @param checkListId
	 * @param GroupName
	 * @return true successful, false fail.
	 */
	public long createNewGroup(String checkListId, String GroupName) {
		long group_id = addRow(DBC.GroupTable.GROUP_COLUMNS, DBC.GroupTable.TABLE_NAME,
				new Object[]{GroupName});
		if (DebugLog.ENABLED) {
			Log.d(LOG_TAG, String.valueOf(group_id));
		}
		long relationId = addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,
				DBC.ItemGroupCheckListTable.TABLE_NAME, new Object[]{checkListId, -1,
						(int) group_id, 0});
		if (relationId != -1) {
			return group_id;
		}
		return -1;
	}

	/**
	 * Delete item through id.
	 * 
	 * @param itemId
	 * @return
	 */
	public boolean deleteItemById(String itemId) {
		sDataBase.beginTransaction();
		String delItem = "delete from item where _id =" + "'" + itemId + "'";
		String updateRelation = "update itemGroupCheckList set item_id = -1 where item_id =" + "'"
				+ itemId + "'";
		// String updateRelation =
		// "delete from itemGroupCheckList where item_id =" + "'" + itemId + "'"
		// + " and checklists_id="+checklist_id;
		try {
			sDataBase.execSQL(delItem);
			sDataBase.execSQL(updateRelation);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();

		return true;
	}

	/**
	 * remove items from checklist, just remove the relationship.
	 * 
	 * @param itemId
	 * @return
	 */
	public boolean removeItemFromChecklist(String checklist_id, String itemId) {

		String updateRelation = "delete from itemGroupCheckList where item_id =" + "'" + itemId
				+ "'" + " and checklists_id=" + checklist_id;
		try {
			sDataBase.execSQL(updateRelation);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Delete group through id.
	 * 
	 * @param groupId
	 * @return
	 */
	public boolean deleteGroupAndItems(String groupId) {
		sDataBase.beginTransaction();
		// DELETE THE GROUP.
		String delGroupStr = "delete from " + DBC.GroupTable.TABLE_NAME + " where " + DBC.ID + "="
				+ groupId;
		// delete relationship.
		String deleteRelation = "delete from itemGroupCheckList where group_id =" + "'" + groupId
				+ "'";

		try {
			sDataBase.execSQL(deleteRelation);
			sDataBase.execSQL(delGroupStr);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
		return true;
	}

	/**
	 * Delete group through id.
	 * 
	 * @param groupId
	 * @return
	 */
	public boolean deleteGroupOnly(String groupId) {
		sDataBase.beginTransaction();
		// DELETE THE GROUP.
		String delGroupStr = "delete from " + DBC.GroupTable.TABLE_NAME + " where " + DBC.ID + "="
				+ groupId;
		// delete relationship.
		String updateRelation = "update itemGroupCheckList set group_id='-1' where group_id ="
				+ "'" + groupId + "'";

		try {
			sDataBase.execSQL(updateRelation);
			sDataBase.execSQL(delGroupStr);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
		return true;
	}

	/**
	 * Add items to specific check list.
	 * 
	 * @param itemIds
	 * @param checkListId
	 * @return
	 */
	public boolean addItemToCheckList(int[] itemIds, int checkListId) {
		sDataBase.beginTransaction();
		for (int i = 0; i < itemIds.length; i++) {
			Object[] inputObjArray = {checkListId, itemIds[i], -1, 0};
			addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,
					DBC.ItemGroupCheckListTable.TABLE_NAME, inputObjArray);
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
		return true;
	}

	/**
	 * Get nonGroupedItem for specific check list.
	 * 
	 * @param checkListId
	 * @return
	 */
	public Cursor getNonGroupedItemsByCheckList(int checkListId) {
		String querystr = "select distinct * from itemGroupCheckList join item where itemGroupCheckList.item_id = item._id and checklists_id="
				+ checkListId + "\u0020and\u0020group_id=-1";
		return sDataBase.rawQuery(querystr, null);
	}

	public Cursor getCheckListById(String checklistid) {
		String querystr = "select * from " + DBC.CheckListsTable.TABLE_NAME + " where _id = "
				+ checklistid;
		return sDataBase.rawQuery(querystr, null);
	}

	public void changeTitle(String table, String titleCloumn, String id, String newTitle) {
		String updateStr = "update " + table + " set " + titleCloumn + "='" + newTitle
				+ "' where _id = " + id;
		sDataBase.execSQL(updateStr);
	}

	public void updateCheckStatus(String checklistId, String itemId, boolean checked) {
		String updateStr = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.IS_CHECKED + "='" + (checked == true ? 1 : 0)
				+ "' where item_id = " + itemId + " and checklists_id=" + checklistId;
		// Log.d(LOG_TAG, "updateStr="+updateStr);
		sDataBase.execSQL(updateStr);
	}

	public void updateFavoriteStatus(String itemId, boolean status) {
		String updateStr = "update " + DBC.ItemTable.TABLE_NAME + " set "
				+ DBC.ItemTable.IS_FAVORITE + "='" + (status == true ? 1 : 0) + "' where " + DBC.ID
				+ " = " + itemId;
		sDataBase.execSQL(updateStr);
	}

	public void updateCheckedStatus(String checklist_id, String itemId, boolean status) {
		String updateStr = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.IS_CHECKED + "='" + (status == true ? 1 : 0)
				+ "' where " + DBC.ItemGroupCheckListTable.ITEM_ID + " = " + itemId
				+ " and checklists_id=" + checklist_id;
		sDataBase.execSQL(updateStr);
	}

	public void updateGroupExpandStatus(String group_id, boolean status) {
		Log.d(LOG_TAG, "groupid:" + group_id);
		String updateStr = " update " + DBC.GroupTable.TABLE_NAME + " set "
				+ DBC.GroupTable.IS_EXPANDED + "='" + (status == true ? 1 : 0) + "' where "
				+ DBC.ID + "=" + group_id;
		sDataBase.execSQL(updateStr);
	}

	public void updateChecklistName(String checklistId, String newTitle) {
		String updateStr = "update " + DBC.CheckListsTable.TABLE_NAME + " set "
				+ DBC.CheckListsTable.TITLE + "='" + newTitle + "'" + " where " + DBC.ID + " = "
				+ checklistId;
		sDataBase.execSQL(updateStr);
	}

	public boolean isExpanded(String group_id) {
		String selectstr = "select " + DBC.GroupTable.IS_EXPANDED + " from "
				+ DBC.GroupTable.TABLE_NAME + " where " + DBC.ID + " = " + group_id;
		Cursor c = sDataBase.rawQuery(selectstr, null);
		if (c.moveToNext()) {
			int ret = c.getInt(c.getColumnIndex(DBC.GroupTable.IS_EXPANDED));
			c.close();
			if (ret == 1) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	public void updateCheckStatusByChecklist(String checklistId, boolean status) {
		String updateStr = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.IS_CHECKED + "='" + (status == true ? 1 : 0)
				+ "' where " + "checklists_id=" + checklistId;
		sDataBase.execSQL(updateStr);
	}

	public void exchangeTwoItemContent(Item startItem, Item destItem) {
		String updateDes = "update " + DBC.ItemTable.TABLE_NAME + " set " + DBC.ItemTable.NAME
				+ "='" + startItem.getmTitle() + "' ,  " + DBC.ItemTable.IS_FAVORITE + "="
				+ (startItem.ismIsFovrite() == true ? 1 : 0) + " where " + DBC.ID + "="
				+ destItem.getmItemId();
		String updateStart = "update " + DBC.ItemTable.TABLE_NAME + " set " + DBC.ItemTable.NAME
				+ "='" + destItem.getmTitle() + "' ,  " + DBC.ItemTable.IS_FAVORITE + "="
				+ (destItem.ismIsFovrite() == true ? 1 : 0) + " where " + DBC.ID + "="
				+ startItem.getmItemId();

		sDataBase.execSQL(updateDes);
		sDataBase.execSQL(updateStart);
	}

	public void exchangeTwoGroupContent(Group startGroup, Group destGroup) {
		String updateDes = "update " + DBC.GroupTable.TABLE_NAME + " set " + DBC.GroupTable.NAME
				+ "='" + startGroup.getmTitle() + "'" + " where " + DBC.ID + "="
				+ destGroup.getmGroupId();

		String updateStart = "update " + DBC.GroupTable.TABLE_NAME + " set " + DBC.GroupTable.NAME
				+ "='" + destGroup.getmTitle() + "'" + " where " + DBC.ID + "="
				+ startGroup.getmGroupId();

		sDataBase.execSQL(updateDes);
		sDataBase.execSQL(updateStart);
	}

	public void exchangeTwoItemRelationship(long startRelationId, Item startItem,
			long destRelationId, Item destItem) {

		String updateDesRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.ITEM_ID + "='" + startItem.getmItemId() + "' , "
				+ DBC.ItemGroupCheckListTable.IS_CHECKED + "="
				+ (startItem.ismChecked() == true ? 1 : 0) + " where " + DBC.ID + "="
				+ destRelationId;

		String updateStartRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.ITEM_ID + "='" + destItem.getmItemId() + "' , "
				+ DBC.ItemGroupCheckListTable.IS_CHECKED + "="
				+ (destItem.ismChecked() == true ? 1 : 0) + " where " + DBC.ID + "="
				+ startRelationId;

		sDataBase.execSQL(updateDesRelation);
		sDataBase.execSQL(updateStartRelation);
	}
	
	public void exchageContentOfTwoRelation(Relationship a, Relationship b){
		
		String updateStartRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
		+ DBC.ItemGroupCheckListTable.ITEM_ID + "=" + b.getItemId() + " , "
		+ DBC.ItemGroupCheckListTable.IS_CHECKED + "="
		+ b.getIsChecked() + " , "+DBC.ItemGroupCheckListTable.GROUP_ID+"="+b.getGroupId()+" , "
		+DBC.ItemGroupCheckListTable.CHECKLISTS_ID+"="+b.getChecklistId()+" where " + DBC.ID + "="
		+ a.getRelationId();
		
		Log.d(LOG_TAG, updateStartRelation);

		String updateDestRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
		+ DBC.ItemGroupCheckListTable.ITEM_ID + "=" + a.getItemId() + " , "
		+ DBC.ItemGroupCheckListTable.IS_CHECKED + "="
		+ a.getIsChecked() + " , "+DBC.ItemGroupCheckListTable.GROUP_ID+"="+a.getGroupId()+" , "
		+DBC.ItemGroupCheckListTable.CHECKLISTS_ID+"="+a.getChecklistId()+" where " + DBC.ID + "="
		+ b.getRelationId();
		
		Log.d(LOG_TAG, updateDestRelation);
		
		sDataBase.beginTransaction();
		sDataBase.execSQL(updateStartRelation);
		sDataBase.execSQL(updateDestRelation);
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
		
	}

	public void exchangeTwoGroupRelationship(ArrayList<Long> startRelationId, Group startGroup,
			ArrayList<Long> destRelationId, Group destGroup) {
		sDataBase.beginTransaction();
		for (Long id : destRelationId) {
		String updateDesRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "=" + startGroup.getmGroupId() + " where "
				+ DBC.ID + "=" + id;
		
			sDataBase.execSQL(updateDesRelation);
		
		}
		for (Long id : startRelationId) {
			String updateStartRelation = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME
					+ " set " + DBC.ItemGroupCheckListTable.GROUP_ID + "="
					+ destGroup.getmGroupId() + " where " + DBC.ID + "=" + id;
			sDataBase.execSQL(updateStartRelation);
		}
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
	}

	public Cursor getRelationId(long checklistId, long groupId, long itemId) {
		String selectStr = "select distinct * from " + DBC.ItemGroupCheckListTable.TABLE_NAME
				+ " WHERE " + DBC.ItemGroupCheckListTable.ITEM_ID + "=" + itemId + " and "
				+ DBC.ItemGroupCheckListTable.CHECKLISTS_ID + "=" + checklistId + " and "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "=" + groupId;
		Log.d(LOG_TAG, "getRelationId:" + selectStr);
		return sDataBase.rawQuery(selectStr, null);

	}

	public Cursor getRelationId(long checklistId, long groupId) {
		String selectStr = "select distinct * from " + DBC.ItemGroupCheckListTable.TABLE_NAME
				+ " WHERE " + DBC.ItemGroupCheckListTable.CHECKLISTS_ID + "=" + checklistId
				+ " and " + DBC.ItemGroupCheckListTable.GROUP_ID + "=" + groupId;
		
		return sDataBase.rawQuery(selectStr, null);

	}

	public void updateGroupIdForItem(long checklistId, long itemId, long oldGroupId, long newGroupId) {
		String updateStr = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "=" + newGroupId + " where "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "=" + oldGroupId + " and "
				+ DBC.ItemGroupCheckListTable.CHECKLISTS_ID + "=" + checklistId + " and "
				+ DBC.ItemGroupCheckListTable.ITEM_ID + "=" + itemId;

		Log.d(LOG_TAG, "updateGroupIdForItem:" + updateStr);
		sDataBase.execSQL(updateStr);
	}

	public long getTheFirstItemIdForGroup(long checklistId, long groupId) {
		String selectStr = "select distinct item._id from " + DBC.ItemTable.TABLE_NAME + " JOIN "
				+ DBC.ItemGroupCheckListTable.TABLE_NAME + " where "
				+ DBC.ItemGroupCheckListTable.CHECKLISTS_ID + "=" + checklistId + " and "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "=" + groupId;

		Cursor c = sDataBase.rawQuery(selectStr, null);
		long firstItemId = 0;
		if (c.moveToNext()) {
			firstItemId = c.getLong(c.getColumnIndex(DBC.ID));
			c.close();
		}
		return firstItemId;
	}
	
	
	public int getNewNumberForDefaultItem(String defaultName){
		String selectStr = "select distinct * from " + DBC.ItemTable.TABLE_NAME + " where "+DBC.ItemTable.NAME+" like '%"+defaultName+"%'";
		Cursor c = sDataBase.rawQuery(selectStr, null);
		int max = -1;
		while(c.moveToNext()) {
			String name = c.getString(c.getColumnIndex(DBC.ItemTable.NAME));
			int tempNumber = Integer.parseInt(name.substring(defaultName.length()));
			if(tempNumber > max){
				max = tempNumber;
			}
		}
		c.close();
		if(max == -1){
			return 1;
		}
		return max+1;
	}
	
	public void beginTransaction(){
		sDataBase.beginTransaction();
	}
	
	public void endTransaction(){
		sDataBase.setTransactionSuccessful();
		sDataBase.endTransaction();
	}
	
	public boolean isChecked(int checklistId, int itemId, int groupId){
		String selectStr = "select distinct * from " + DBC.ItemGroupCheckListTable.TABLE_NAME + " where "+DBC.ItemGroupCheckListTable.ITEM_ID+"="+itemId+
		" and "+DBC.ItemGroupCheckListTable.CHECKLISTS_ID+"="+checklistId+" and "+ DBC.ItemGroupCheckListTable.GROUP_ID+"="+groupId;
		Cursor c = sDataBase.rawQuery(selectStr,null);
		if(c.moveToNext()){
			boolean checked = c.getInt(c.getColumnIndex(DBC.ItemGroupCheckListTable.IS_CHECKED)) == 1?true:false;
			return checked;
		}
		return false;
	}
	
	public void releaseDatabase(){
		
		if(sDataBase != null && sDataBase.isOpen()){
			sDataBase.close();
			sDataBase = null;
		}
		
		if(sDBHelper != null){
			sDBHelper.close();
			sDBHelper = null;
		}
		
		if(sDataBaseHandler != null){
			sDataBaseHandler = null;
		}
	}
	
	/**
	 * add specific item to specific group.
	 * @param itemId which item need to added to group.
	 * @param groupId which group needed to be added in.
	 */
	public void addItemToGroup(int itemId,int groupId, int checklistId){
		Log.d("addItemToGroup:", "itemId--"+itemId);
		Log.d("addItemToGroup:", "checklistId--"+checklistId);
		Log.d("addItemToGroup:", "groupId--"+groupId);
		String updateStr = "update " + DBC.ItemGroupCheckListTable.TABLE_NAME + " set "
				+ DBC.ItemGroupCheckListTable.GROUP_ID + "='" + groupId
				+ "' where item_id = " + itemId +" and " + DBC.ItemGroupCheckListTable.CHECKLISTS_ID + " = "+"'"+checklistId+"'" ;
		sDataBase.execSQL(updateStr);
	}
	
}
