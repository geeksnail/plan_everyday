package com.springnap.checklist;

import android.content.ContentValues;
import android.util.Log;

public class DBC {
    
    private final static String LOG_TAG = "DBConst";
    public final static String ID = "_id";
    public final static String PRIMARY_KEY = "INTEGER PRIMARY KEY";
    public final static String FOREIGN_KEY = "FOREIGN KEY";
    public static final String DATABASE_NAME = "ifrejseforsikringer_checklist.db";
    public final static String DATABASE_RELATIVE_LOCATION="/data/com.tieto/databases/" + DATABASE_NAME;    
    
    private final static String SQL_FIELD_TYPE_TEXT="TEXT";
    private final static String SQL_FIELD_TYPE_INTEGER="INTEGER"; 
    
    public final static String SELECT = " SELECT ";
    public final static String DELETE = " DELETE ";
    public final static String FROM = " FROM ";
    public final static String WHERE = " WHERE ";
    public final static String STAR = " * ";
    
    public static String getCreateTableString(String tableName, DBColumn[] columns) {		
		StringBuilder createTable = new StringBuilder();
		createTable.append("CREATE TABLE IF NOT EXISTS " + tableName + " (");
		
		for (int i = 0; i < columns.length; i++) {
			
			if(columns[i].getDBType().equals(FOREIGN_KEY)){
				//If foreign key, then only one string is required
				createTable.append((columns[i].getColumn()));
			}else{
				createTable.append((columns[i].getColumn() + " " + columns[i].getDBType()));
			}
			
			if (i != columns.length - 1) {
			    createTable.append(",");
			}
		}
		
		createTable.append(");");
		Log.d(LOG_TAG, "Creating database table from String: "
				+ createTable);

		return createTable.toString();
	}
    
	public static ContentValues getContentValues(DBColumn[] columns, Object[] valuesToInsert) {
		ContentValues cValues = new ContentValues();
		for (int i = 0; i < valuesToInsert.length ; i++) {
		    
		    if(valuesToInsert[i] instanceof String){
		        cValues.put(columns[i + 1].getColumn(), (String)valuesToInsert[i]);
		    }else if(valuesToInsert[i] instanceof Integer){
		        cValues.put(columns[i + 1].getColumn(), (Integer)valuesToInsert[i]);
		    }			
		}
		return cValues;
	}
		

	public static final class CheckListsTable {
		public static final String TABLE_NAME = "checkLists";		
		
		public static final String TITLE = "title";														
		public static final DBColumn[] CHECKLIST_COLUMNS = {
				new DBColumn(ID, PRIMARY_KEY), 
				new DBColumn(TITLE, SQL_FIELD_TYPE_TEXT),
		};
	}
	
	public static final class ItemTable {
		public static final String TABLE_NAME = "item";	
		public static final String NAME = "name";
		public static final String IS_FAVORITE = "is_favorite";	
		
		public static final DBColumn[] ITEMSLIST_COLUMNS = {
				new DBColumn(ID, PRIMARY_KEY), 
				new DBColumn(NAME, SQL_FIELD_TYPE_TEXT),
				new DBColumn(IS_FAVORITE, SQL_FIELD_TYPE_INTEGER)
			};				
	}
	
	public static final class ItemGroupCheckListTable {
		public static final String TABLE_NAME = "itemGroupCheckList";	
		
		public static final String CHECKLISTS_ID = "checklists_id";
		public static final String ITEM_ID = "item_id";
		public static final String GROUP_ID = "group_id";
		public static final String IS_CHECKED = "is_checked";
		
		public static final DBColumn[] ITEMS_COLUMNS = {
				new DBColumn(ID, PRIMARY_KEY), 
				new DBColumn(CHECKLISTS_ID, SQL_FIELD_TYPE_INTEGER),
				new DBColumn(ITEM_ID, SQL_FIELD_TYPE_INTEGER),
				new DBColumn(GROUP_ID, SQL_FIELD_TYPE_INTEGER),
				new DBColumn(IS_CHECKED, SQL_FIELD_TYPE_INTEGER)
		};
	}
	
	public static final class GroupTable {
		public static final String TABLE_NAME = "groups";	
		public static final String NAME = "group_name";																		
		public static final String IS_EXPANDED = "is_expanded";
		
		public static final DBColumn[] GROUP_COLUMNS = {
				new DBColumn(ID, PRIMARY_KEY), 
				new DBColumn(NAME, SQL_FIELD_TYPE_TEXT),
				new DBColumn(IS_EXPANDED, SQL_FIELD_TYPE_INTEGER)};		
	}
	
	//
	//	Other databases would in this setup be added here as static inner classes
	//	
}
