package com.springnap.checklist;

public class DBColumn {
    private String column;
    private String DBType;
    
    public DBColumn(String column, String DBType){
        this.column=column;
        this.DBType=DBType;
    }
    
    /**
     * @return the column
     */
    public String getColumn() {
        return column;
    }
    /**
     * @param column the column to set
     */
    public void setColumn(String column) {
        this.column = column;
    }
    /**
     * @return the dBType
     */
    public String getDBType() {
        return DBType;
    }
    /**
     * @param dBType the dBType to set
     */
    public void setDBType(String dBType) {
        DBType = dBType;
    }
    
    
}
