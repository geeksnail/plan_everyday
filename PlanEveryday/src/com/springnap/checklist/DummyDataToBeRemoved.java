package com.springnap.checklist;

public class DummyDataToBeRemoved {
    public void populateDatabase(ChecklistDatabaseHandler cdh){
        cdh.addRow(DBC.CheckListsTable.CHECKLIST_COLUMNS,DBC.CheckListsTable.TABLE_NAME, new Object[]{"dummyChecklist1"});
        cdh.addRow(DBC.CheckListsTable.CHECKLIST_COLUMNS,DBC.CheckListsTable.TABLE_NAME, new Object[]{"dummyChecklist2"});
        cdh.addRow(DBC.CheckListsTable.CHECKLIST_COLUMNS,DBC.CheckListsTable.TABLE_NAME, new Object[]{"dummyChecklist3"});
        
        cdh.addRow(DBC.GroupTable.GROUP_COLUMNS,DBC.GroupTable.TABLE_NAME, new Object[]{"dummyGroup1"});
        cdh.addRow(DBC.GroupTable.GROUP_COLUMNS,DBC.GroupTable.TABLE_NAME, new Object[]{"dummyGroup2"});
        cdh.addRow(DBC.GroupTable.GROUP_COLUMNS,DBC.GroupTable.TABLE_NAME, new Object[]{"dummyGroup3"});
        
        cdh.addRow(DBC.ItemTable.ITEMSLIST_COLUMNS,DBC.ItemTable.TABLE_NAME, new Object[]{"dummyItem1",null});
        cdh.addRow(DBC.ItemTable.ITEMSLIST_COLUMNS,DBC.ItemTable.TABLE_NAME, new Object[]{"dummyItem2",1});
        cdh.addRow(DBC.ItemTable.ITEMSLIST_COLUMNS,DBC.ItemTable.TABLE_NAME, new Object[]{"dummyItem3",1});
             
        cdh.addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,DBC.ItemGroupCheckListTable.TABLE_NAME, new Object[]{1,1,1,1});
        cdh.addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,DBC.ItemGroupCheckListTable.TABLE_NAME, new Object[]{1,2,1,1});
        cdh.addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS,DBC.ItemGroupCheckListTable.TABLE_NAME, new Object[]{3,1,2,1});
        
    }
}
