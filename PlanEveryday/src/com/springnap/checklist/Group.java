package com.springnap.checklist;

import java.util.ArrayList;

public class Group {
	private long mGroupId;
	private String mTitle;
    private ArrayList<Item> mItems;
    private boolean mCollapsed;
    
    public boolean ismCollapsed() {
        return mCollapsed;
    }
    public void setmCollapsed(boolean mCollapsed) {
        this.mCollapsed = mCollapsed;
    }
    public String getmTitle() {
        return mTitle;
    }
    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }
    public ArrayList<Item> getmItems() {
        return mItems;
    }
    public void setmItems(ArrayList<Item> mItems) {
        this.mItems = mItems;
    }
    public long getmGroupId() {
		return mGroupId;
	}
	public void setmGroupId(long mGroupId) {
		this.mGroupId = mGroupId;
	}
}
