package com.springnap.checklist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.springnap.checklist.util.AbstractActivity;

public class HelpActivity extends AbstractActivity {

	public static final String IS_AFTER_FIRST_CREATE = "after_first_create";
	@Override
    public int getContentLayout() {
	    return R.layout.help;
    }

	@Override
    public CharSequence getTitleText() {
	   
	    return this.getString(R.string.CheckListView_HELP_TITLE);
    }

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		boolean afterFirstCreate = this.getIntent().getBooleanExtra(IS_AFTER_FIRST_CREATE, false);
		Button btn_continue = (Button)findViewById(R.id.goon);
		if(afterFirstCreate){
			btn_continue.setOnClickListener(new OnClickListener(){
	
				@Override
	            public void onClick(View v) {
		            Intent openCheckList = new Intent();
		            openCheckList.setClass(HelpActivity.this, CheckListDetailActivity.class);
		            openCheckList.putExtra(CheckListDetailActivity.MODE, CheckListDetailActivity.MODE_CREATE_NEW);
		            HelpActivity.this.startActivity(openCheckList);
		            
		            HelpActivity.this.finish();
	            }
				
			});
		} else {
			btn_continue.setVisibility(View.GONE);
		}
	   
    }
}
