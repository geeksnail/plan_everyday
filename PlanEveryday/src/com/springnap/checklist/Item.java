package com.springnap.checklist;

public class Item {
    
    private boolean mChecked;
    private String mTitle;
    private boolean mIsFovrite;
    private long mItemId;
    
    public long getmItemId() {
		return mItemId;
	}
	public void setmItemId(long mItemId) {
		this.mItemId = mItemId;
	}
	public boolean ismChecked() {
        return mChecked;
    }
    public void setmChecked(boolean mChecked) {
        this.mChecked = mChecked;
    }
    public String getmTitle() {
        return mTitle;
    }
    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }
    public boolean ismIsFovrite() {
        return mIsFovrite;
    }
    public void setmIsFovrite(boolean mIsFovrite) {
        this.mIsFovrite = mIsFovrite;
    }

}
