package com.springnap.checklist;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.mobclick.android.MobclickAgent;
import com.springnap.checklist.ItemListAdapter.OnEditModeChangedListener;
import com.springnap.checklist.ItemListAdapter.OnItemNameChangedListener;
import com.springnap.checklist.util.AbstractActivity;

public class ItemListActivity extends AbstractActivity implements
OnClickListener {
	public static final String SELECTED_ITEM = "selected_item";
	public static final String EXISTING_ITEM = "existing_item";
	
	public static final String REQUEST_MODE = "request_mode";
	public static final String MODE_ADD_TO_CHECKLIST = "add_to_checklist";
	public static final String MODE_MANAGE = "manage_item";
	public static final String CHECKLIST_ID = "checklist_id";
	public static final String GROUP_ID = "group_id";

	protected static final String TAG = "ItemListActivity";
	private ItemListAdapter itemListAdapter;
	private Cursor mCur_ItemList;
	ChecklistDatabaseHandler dbHandler;
	ListView itemList;
	
    @Override
	protected void onDestroy() {
		if (mCur_ItemList != null){
			mCur_ItemList.close();
		}
		super.onDestroy();
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        Intent intent = this.getIntent();
        String mode = intent.getStringExtra(REQUEST_MODE);
        /** The item list was launched from check list. */
        /** Get item list info from database.*/
        dbHandler = ChecklistDatabaseHandler.getInstance(this);
        mCur_ItemList = dbHandler.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);
        itemListAdapter = new ItemListAdapter(this, mCur_ItemList, mode);
        itemList = (ListView)findViewById(R.id.itemList);
        
        if(mode.equals(MODE_ADD_TO_CHECKLIST)){
            ArrayList<Integer> existingItems = intent.getIntegerArrayListExtra(EXISTING_ITEM);
            itemListAdapter.setExistingItems(existingItems);
            itemListAdapter.setmCheckListId(Integer.parseInt(intent.getStringExtra(CHECKLIST_ID)));
            itemListAdapter.setmGroupId(Integer.parseInt(intent.getStringExtra(GROUP_ID)));
            itemList.setAdapter(itemListAdapter);
            
        } else {// The manage mode.
        	 itemList.setAdapter(itemListAdapter);
        }
        
        /**
         * Listener for the edit mode change.
         */
        OnEditModeChangedListener listener = new OnEditModeChangedListener(){
			@Override
			public void onEditModeEnabled() {
			}

			@Override
			public void onEditModeDisabled() {
			}
		};
		
		/**
         * Listener for the itemName change.
         */
        OnItemNameChangedListener itemChangedlistener = new OnItemNameChangedListener(){
			@Override
			public void onItemNameChanged(int position) {
				itemList.setSelection(position);
			}
		};
		
		//Set the call-back for go into the edit mode.
		itemListAdapter.setOnEditModeEnabledListener(listener);
		itemListAdapter.setOnItemNameChangedListener(itemChangedlistener);
		
		this.findViewById(R.id.btn_add_item).setOnClickListener(this);
		((ImageView)this.findViewById(R.id.btn_add_item)).setImageResource(R.drawable.add_new_item);
		this.findViewById(R.id.btn_edit).setOnClickListener(this);
    }

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
	
	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) ItemListActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}
	
	private int getPosition(Cursor cursor, String name){
		while(cursor.moveToNext()){
			String tempItemName = cursor.getString(cursor.getColumnIndex(DBC.ItemTable.NAME));
			if(tempItemName.equals(name)){
				return cursor.getPosition();
			}
		}
		return cursor.getCount()-1;
	}
	
    @Override
    public int getContentLayout() {
        return R.layout.item_list;
    }

    @Override
    public CharSequence getTitleText() {
        return this.getString(R.string.CheckListView_ITEM_LIST_TITLE);
    }


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if (itemListAdapter.getEditMode()){
				itemListAdapter.setEditMode();
				itemListAdapter.notifyDataSetChanged();
				return true;
			}
			Intent data = new Intent();
			if(itemListAdapter.mNeedScrollToTop){
				// add the item IDS that be selected.
				ItemListActivity.this.setResult(Activity.RESULT_OK, data);
			} else {
				ItemListActivity.this.setResult(Activity.RESULT_CANCELED, data);
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_add_item:
			AlertDialog.Builder builder = new AlertDialog.Builder(ItemListActivity.this);
			builder.setMessage(ItemListActivity.this.getString(R.string.CheckListView_ADD_ITEM));
			final EditText et = new EditText(ItemListActivity.this);
			builder.setView(et);
			builder.setPositiveButton(this.getString(R.string.Generic_ADD),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							String newName;
							if(et.getText() != null && et.getText().length() != 0){
								newName = et.getText().toString();
							}else{
								// Add a item to the item table.
								String defaultName = ItemListActivity.this.getString(R.string.CheckListView_ADD_ITEM_DEFAULT_NAME);
								newName = defaultName+dbHandler.getNewNumberForDefaultItem(defaultName);
							}
							Object[] inputObjArray = {newName,-1};
							dbHandler.addRow(DBC.ItemTable.ITEMSLIST_COLUMNS, DBC.ItemTable.TABLE_NAME, inputObjArray);
							Cursor cur_item = dbHandler.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);
							itemListAdapter.changeCursor(cur_item);
							//Calculate the position of the new item, and scroll to that position(use selection).
							int position = getPosition(cur_item,newName);
							itemList.setSelection(position);
						}
					});
			builder.setNegativeButton(this.getString(R.string.Generic_CANCEL),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
					}
			});
			builder.create();
			builder.show();
			openKeyboard();
			break;
		case R.id.btn_edit:
			itemListAdapter.setEditMode();
			itemListAdapter.notifyDataSetChanged();
			break;
		}
	}
}