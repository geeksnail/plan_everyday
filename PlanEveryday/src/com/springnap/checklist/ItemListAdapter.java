package com.springnap.checklist;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ItemListAdapter extends CursorAdapter {
	
	private static final String TAG = "ItemListAdapter";
	private LayoutInflater mInflater = null;
	private ChecklistDatabaseHandler mDatabaseHanlder;
	private boolean mInEditMode = false;
	private Activity mBoundary;
	private String mMode;
	private int mCheckListId;
	public boolean mNeedScrollToTop;
	private OnEditModeChangedListener mEditModeListener;
	private OnItemNameChangedListener mItemNameChangedListener;
	
	// Store the status of the existing items.
	private ArrayList<Integer> existingItem = new ArrayList<Integer>();
	private int mGroupId;

	/** Listener for edit mode enabled. */
	interface OnEditModeChangedListener {
		void onEditModeEnabled();
		void onEditModeDisabled();
	}
	
	interface OnItemNameChangedListener {
		void onItemNameChanged(int position);
	}
	
	public int getmCheckListId() {
    	return mCheckListId;
    }

	public void setmCheckListId(int mCheckListId) {
    	this.mCheckListId = mCheckListId;
    }
	
	public void setmGroupId(int mGroupId) {
    	this.mGroupId = mGroupId;
    }

	public void setOnEditModeEnabledListener(OnEditModeChangedListener l) {
		mEditModeListener = l;
	}
	
	public void setOnItemNameChangedListener(OnItemNameChangedListener l) {
		mItemNameChangedListener = l;
	}
	
	public ItemListAdapter(Context context, Cursor c, String mode) {
		super(context, c);
		mBoundary = (Activity) context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDatabaseHanlder = ChecklistDatabaseHandler.getInstance(context);
		mMode = mode;
	}
	

	@Override
	public void bindView(View view, Context context, final Cursor cursor) {
		final int item_id = cursor.getInt(cursor.getColumnIndex(DBC.ID));
        final String ItemList_item_name = cursor.getString(cursor.getColumnIndex(DBC.ItemTable.NAME));
        
		TextView itemName = (TextView) view.findViewById(R.id.ItemList_item_name);
		CheckBox addBox = (CheckBox) view.findViewById(R.id.ItemList_add_item_checkbox);
		
		if (mMode.equals(ItemListActivity.MODE_ADD_TO_CHECKLIST)) {
			addBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// the check list detail page will need to scroll to top.
					mNeedScrollToTop = true;
					if(existingItem.contains(item_id)){
						// remove the item relation from database.
						mDatabaseHanlder.removeItemFromChecklist(String.valueOf(mCheckListId), String.valueOf(item_id));
						existingItem.remove(new Integer(item_id));
					}else{
						// add the item relation to database.
						Object[] inputObjArray = {mCheckListId,item_id,mGroupId,0};
						mDatabaseHanlder.addRow(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS, DBC.ItemGroupCheckListTable.TABLE_NAME, inputObjArray);
						existingItem.add(item_id);
					}
					Cursor cur_item_list = mDatabaseHanlder.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);
					// update cursor, update list.
					changeCursor(cur_item_list);
					notifyDataSetChanged();
				}

			});

			
			/**
			 * If the item has been existing in the current checkList, the
			 * background will be gray, and the item will be un-clickable.
			 */
			view.setBackgroundColor(Color.WHITE);
			addBox.setClickable(true);
			addBox.setChecked(false);
			itemName.setTextColor(Color.BLACK);
			if (existingItem != null) {
				for (int i = 0; i < existingItem.size(); i++) {
					if (existingItem.get(i) == item_id) {
						addBox.setChecked(true);
						itemName.setTextColor(Color.rgb(178, 178, 178));
						addBox.setClickable(false);
						break;
					}
				}
			}
			
		} else {
			view.setClickable(false);
		}
		
		
		if (mInEditMode == true) {
			view.findViewById(R.id.ItemList_delete_item).setVisibility(View.VISIBLE);

			view.findViewById(R.id.ItemList_delete_item).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							AlertDialog.Builder builder = new AlertDialog.Builder(
									mBoundary);
							builder.setMessage(mBoundary
									.getString(R.string.CheckListView_DELETE_CONFIRM)+"\n"+
									ItemList_item_name);
							
							builder.setPositiveButton(
									v.getContext().getString(R.string.Generic_DELETE),
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int whichButton) {
											// Delete a item.
											mDatabaseHanlder.deleteItemById(String
													.valueOf(item_id));
											Cursor Cur_ItemList = mDatabaseHanlder
													.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);

											// update cursor, update list.
											ItemListAdapter.this.changeCursor(Cur_ItemList);
											ItemListAdapter.this.notifyDataSetChanged();
										}
									});

							builder.setNegativeButton(v.getContext().getString(R.string.Generic_CANCEL),null);
							builder.create();
							builder.show();
						}

					});

			view.findViewById(R.id.ItemList_item_name).setBackgroundResource(R.drawable.editbox);
			// Set listener for item.
			view.findViewById(R.id.ItemList_item_name).setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(final View v) {
							final LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.input_box, null);
							final EditText editbox = (EditText) view.findViewById(R.id.input);
							editbox.setText(ItemList_item_name);
							editbox.setSelection(ItemList_item_name.length());
							AlertDialog.Builder builder = new AlertDialog.Builder(mBoundary);

							builder.setMessage(mBoundary.getString(R.string.CheckListView_CHANGE_ITEM_TEXT));
							builder.setView(view);
							builder.setPositiveButton(
									v.getContext().getString(
											R.string.Generic_EDIT),
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int whichButton) {

											ChecklistDatabaseHandler databaseHandler = ChecklistDatabaseHandler.getInstance(v.getContext());
											String newTitle = editbox.getEditableText().toString();
											databaseHandler.changeTitle(
															DBC.ItemTable.TABLE_NAME,
															DBC.ItemTable.NAME,
															String.valueOf(item_id),
															newTitle);

											// need refresh the check list
											// when
											// return from the dialog.
											Cursor Cur_ItemList = mDatabaseHanlder.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);

											// update cursor, update list.
											ItemListAdapter.this.changeCursor(Cur_ItemList);
											ItemListAdapter.this.notifyDataSetChanged();
											
											mItemNameChangedListener.onItemNameChanged(getItemPositionByName(newTitle));
											
										}

										private int getItemPositionByName(String newTitle) {
											Cursor cur_ItemList = mDatabaseHanlder.getCursor(DBC.ItemTable.TABLE_NAME,DBC.ItemTable.NAME);
											while(cur_ItemList.moveToNext()){
												String name = cur_ItemList.getString(cur_ItemList.getColumnIndex(DBC.ItemTable.NAME));
												if(name.equals(newTitle)){
													Log.d(TAG, "name:"+name);
													Log.d(TAG, "newTitle:"+newTitle);
													return cur_ItemList.getPosition();
												}
											}
											int lastIndex = cur_ItemList.getCount()-1;
											cur_ItemList.close();
											
											return lastIndex;
										}
									});

							builder.setNegativeButton(v.getContext().getString(R.string.Generic_CANCEL),null);
							builder.create();
							builder.show();
							openKeyboard();
						}

					});

		} else {
			view.findViewById(R.id.ItemList_delete_item).setVisibility(View.GONE);
			view.findViewById(R.id.ItemList_item_name).setClickable(false);
			view.findViewById(R.id.ItemList_item_name).setBackgroundDrawable(null);
		}
		
		itemName.setText(cursor.getString(cursor.getColumnIndex(DBC.ItemTable.NAME)));
	}


	public boolean setExistingItems(ArrayList<Integer> existingItems) {
		this.existingItem = existingItems;
		return true;
	}
	

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return mInflater.inflate(R.layout.item_row, null);
	}

	public void setEditMode() {
		mInEditMode = !mInEditMode;
		if (mInEditMode == true && mEditModeListener != null) {
			mEditModeListener.onEditModeEnabled();
		} else if (mInEditMode == false && mEditModeListener != null) {
			mEditModeListener.onEditModeDisabled();
		}
	}

	public boolean getEditMode() {
		return mInEditMode;
	}

	/**
	 * Open keyboard after pop up a window for item name change.
	 */
	private void openKeyboard() {

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                @Override
                public void run() {
                        InputMethodManager imm = (InputMethodManager) mBoundary.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                }
        }, 200);
	}
}
