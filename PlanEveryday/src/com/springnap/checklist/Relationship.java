/**
 * 
 */
package com.springnap.checklist;

/**
 * @author Administrator
 *
 */
public class Relationship {
	private int relationId;
	private int checklistId;
	private int groupId;
	private int itemId;
	private int isChecked;
	/**
	 * @return the relationId
	 */
	public int getRelationId() {
		return relationId;
	}
	/**
	 * @param relationId the relationId to set
	 */
	public void setRelationId(int relationId) {
		this.relationId = relationId;
	}
	/**
	 * @return the checklistId
	 */
	public int getChecklistId() {
		return checklistId;
	}
	/**
	 * @param checklistId the checklistId to set
	 */
	public void setChecklistId(int checklistId) {
		this.checklistId = checklistId;
	}
	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	/**
	 * @return the isChecked
	 */
	public int getIsChecked() {
		return isChecked;
	}
	/**
	 * @param isChecked the isChecked to set
	 */
	public void setIsChecked(int isChecked) {
		this.isChecked = isChecked;
	}
	
}
