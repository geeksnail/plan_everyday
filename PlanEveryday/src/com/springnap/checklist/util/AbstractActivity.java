package com.springnap.checklist.util;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.springnap.checklist.R;

/**
 * Abstract super class used by most activities in this application.
 * Takes care of setting the correct title (logo), and uses the correct
 * layout or view depending on the subclass.
 * 
 * @see getContentLayout and getTitleText below
 */
public abstract class AbstractActivity extends Activity {

    private static final String LOG_TAG = AbstractActivity.class.getSimpleName();
    protected View mContentView = null;

    /**
     * Initializes activity with custom title bar, using label (title) defined in manifest for
     * given activity and loading the provided layout.
     * 
     * @param savedInstanceState
     * @param layout the layout xml file to load (eg: R.layout.view_locations) 
     */
    protected void onCreate(Bundle savedInstanceState) {        
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        
        setContentView(getContentLayout());

        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_bar);
        
        // Set correct title or logo (from manifest label).
        TextView titleText = (TextView) findViewById(R.id.title_text);
        
        
        if (titleText != null) {
        	CharSequence title = getTitleText();
        	if( title != null){
        		titleText.setText(title);
        		titleText.setTransformationMethod(SingleLineTransformationMethod.getInstance());
        		titleText.setSelected(true);
        	}
        	titleText.setVisibility(View.VISIBLE);

        } else {
            if (DebugLog.ENABLED) {
                Log.v(LOG_TAG, "Did you forget to add a theme to the activity "
                        + getLocalClassName() + " in the manifest?");
            }
        }
    }

    /**
     * Abstract method called by AbstractActivity to get the correct custom
     * title bar text. If null is returned, the ifrejseforsikringer logo is shown instead.
     * Implemented by subclasses of AbstractActivity.
     * 
     * @return title as CharSequence or null if a logo should be shown
     */
    public abstract CharSequence getTitleText();

    /**
     * Abstract method called by onCreate, and implemented by subclasses. Correct
     * implementation will be reached using polymorphism. This method gets the
     * layout ID that should be the content view for this activity.
     *
     * @return layout id (like R.id.mylayout)
     */
    public abstract int getContentLayout();
}
