package com.springnap.checklist.util;

import android.app.Application;
import android.database.Cursor;
import android.util.Log;
import com.springnap.checklist.ChecklistDatabaseHandler;
import com.springnap.checklist.DBC;

/**
 * Override of the default Application. This is used in AndroidManifest.xml.
 * This approach give "global" objects that are accessible in all Activities.
 */
public class ChecklistApplication extends Application {

    private static final String LOG_TAG = ChecklistApplication.class.getSimpleName();
    
  
    private long mLastShowedTime = 0;
    
    /**
     * @return the mLastShowedTime
     */
    public long getmLastShowedTime() {
        return mLastShowedTime;
    }

    /**
     * @param mLastShowedTime the mLastShowedTime to set
     */
    public void setmLastShowedTime(long mLastShowedTime) {
        this.mLastShowedTime = mLastShowedTime;
    }

    @Override
    public void onCreate() {        
        super.onCreate();
       
        if (DebugLog.ENABLED) {
            Log.d(LOG_TAG, "Application onCreate");
        }
        
        checkCheckListDatabase();
    }
    
    private void checkCheckListDatabase(){
    	final ChecklistDatabaseHandler checkListDatabase = ChecklistDatabaseHandler.getInstance(this);
    	// table checklists
    	checkListDatabase.createTable(DBC.CheckListsTable.CHECKLIST_COLUMNS, DBC.CheckListsTable.TABLE_NAME);
    	
    	// table item
    	checkListDatabase.createTable(DBC.ItemTable.ITEMSLIST_COLUMNS, DBC.ItemTable.TABLE_NAME);
    	
    	// table groups
    	checkListDatabase.createTable(DBC.GroupTable.GROUP_COLUMNS, DBC.GroupTable.TABLE_NAME);
    	
    	// table relations
    	checkListDatabase.createTable(DBC.ItemGroupCheckListTable.ITEMS_COLUMNS, DBC.ItemGroupCheckListTable.TABLE_NAME);
    	
    	// populate the predefined items.
    	Cursor c = checkListDatabase.getCursor(DBC.ItemTable.TABLE_NAME);
    	if(c == null || c.getCount() == 0){
//    	    new Thread(){
//    	    	public void run(){
    				 c.close();
    				 
    	    		 checkListDatabase.populatePredefinedItems();
    	    		 
//    	    	}
//    	    }.start();
    	}else{
    		 c.close();
    	}
    }

}
